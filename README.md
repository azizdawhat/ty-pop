# ty-pop

✨ Make web typography pop!

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

## Installation

```bash
npx degit gitlab:azizdawhat/ty-pop ./ty-pop
```

```bash
cd ./ty-pop
```

```bash
npm i
```

## Authors

Aziz Da <azizdawhat@gmail.com>

## License

[GPL-3.0-or-later](COPYING)

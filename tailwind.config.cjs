/** @type {import('tailwindcss').Config} */
const config = {
  content: ['./app/*.html'],
};

module.exports = config;

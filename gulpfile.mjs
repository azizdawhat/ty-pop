import '@babel/register';

import { create as createServer } from 'browser-sync';

import { watch } from 'chokidar';

import { deleteAsync as del } from 'del';

import gulp from 'gulp';

import postCSS from 'gulp-postcss';

import { on as addListener } from 'node:events';

import { createRequire } from 'node:module';

import { env as processEnv } from 'node:process';

import { pipeline } from 'node:stream/promises';

import { promisify } from 'node:util';

import { rollup, watch as rollupWatch } from 'rollup';

import rollupOptions from './rollup.config.mjs';

const require = createRequire(import.meta.url);

const server = createServer();

const { dest, lastRun, parallel, series, src } = gulp;

/** @type {{NODE_ENV: 'prod'}} */
const { NODE_ENV = 'prod' } = processEnv;

/** @type {import('browser-sync').Options} */
const serverOptions = require('./bs-config.cjs');

/** @type {{config: {plugins: Array<import('postcss').AcceptedPlugin>}}} */ // prettier-ignore
const { config: { plugins: postCSSPlugins } } = require('./postcss.config.cjs');

/**
 * @template T, A, R
 * @param {function(this: T, ...A): R} func
 * @param {T} thisArg
 * @returns {function(...A): R}
 * @see {@link https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-0.html#generic-rest-parameters}
 */
function bind(func, thisArg) {
  return func.bind(thisArg);
}
// prettier-ignore
const build = series(...[
  remove,
  parallel(script, styleLint, style),
].concat(NODE_ENV === 'prod' ? [
  copyHTML,
] : []));

/**
 * @template T
 * @param {(Array<T>|T)} value
 */
function castArray(value) {
  return Array.isArray(value) ? value : Array.of(value);
}

function copyHTML() {
  return pipeline(
    src(['./app/*.html']),
    dest('./dist'),
    // ,
  );
}

/**
 * @template A, R
 * @param {Iterable<A>} iterable
 * @param {function(A): Promise<R>} callbackFn
 */
function map(iterable, callbackFn) {
  return Promise.all(
    Array.from(iterable, callbackFn),
    // ,
  );
}

/**
 * @param {AsyncIterableIterator} iterable
 * @param {function} callbackFn
 */
async function* mapAsync(iterable, callbackFn) {
  for await (const value of iterable) {
    yield callbackFn(...castArray(value));
  }
}

function remove() {
  return del(['./dist/*']);
}

function script() {
  return map(castArray(rollupOptions), async function callbackFn({ output, watch, ...input }) {
    const build = await rollup(input);

    await map(castArray(output), function callbackFn(element) {
      return build.write(element);
    });

    return build.close();
  });
}

async function scriptWatch() {
  const watcher = rollupWatch(rollupOptions);
  // prettier-ignore
  for await (const value of mapAsync(addListener(watcher, 'event'), series(scriptWatchListener, bind(server.reload, server)))) {}

  return watcher.close();
}

/** @param {import('rollup').RollupWatcherEvent} event */
async function scriptWatchListener(event) {
  if (event.code === 'BUNDLE_END') {
    await event.result.close();
  }
}
// prettier-ignore
const serve = series(...[
  build,
  startServer,
].concat(NODE_ENV === 'prod' ? [] : [
  parallel(scriptWatch, styleWatch),
]));

function startServer() {
  return bind(promisify(server.init), server)(serverOptions);
}

function style() {
  return pipeline(
    src(['./app/app.css'], {
      since: lastRun(style),
      sourcemaps: true,
    }),
    postCSS(),
    dest('./dist', {
      sourcemaps: true,
    }),
    server.stream(),
  );
}

function styleLint() {
  return pipeline(
    src(['./app/*.css'], {
      since: lastRun(styleLint),
    }),
    postCSS(postCSSPlugins),
  );
}

async function styleWatch() {
  const watcher = watch(['./app/*.css']);
  // prettier-ignore
  for await (const value of mapAsync(addListener(watcher, 'change'), parallel(styleLint, style))) {}

  return watcher.close();
}

export { build, remove, serve };

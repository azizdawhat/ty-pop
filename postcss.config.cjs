const autoPrefixer = require('autoprefixer');

const cssNano = require('cssnano');

/** @type {{env: {NODE_ENV: 'prod'}}} */ // prettier-ignore
const { env: { NODE_ENV = 'prod' } } = require('node:process');

const importRule = require('postcss-import');

const reporter = require('postcss-reporter');

const styleLint = require('stylelint');

const tailwindCSS = require('tailwindcss');

const { plugins } = cssNano({ preset: 'default' });

const { resolve: requireResolve } = require;

/** @type {{plugins: Array<import('postcss').AcceptedPlugin>}} */
const config = {
  // prettier-ignore
  plugins: [
    importRule({
      resolve(id, baseDir) {
        return requireResolve(id, {
          paths: [baseDir, './node_modules'],
        });
      },
    }),
    tailwindCSS({
      config: './tailwind.config.cjs',
    }),
  ].concat(NODE_ENV === 'prod' ? [
    autoPrefixer(),
  ].concat(plugins) : []),
};

/** @type {{plugins: Array<import('postcss').AcceptedPlugin>}} */
const value = {
  plugins: [
    styleLint({
      formatter: 'string',
      quietDeprecationWarnings: true,
    }),
    reporter({
      clearReportedMessages: true,
    }),
  ],
};

module.exports = Object.defineProperty(config, 'config', { value });

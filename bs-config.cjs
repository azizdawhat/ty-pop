const { format: formatPath, relative: relativePath } = require('node:path');

/** @type {{env: {NODE_ENV: 'prod'}}} */ // prettier-ignore
const { env: { NODE_ENV = 'prod' } } = require('node:process');

const { name } = require('./package.json');

/** @type {import('browser-sync').Options} */
const devSource = {
  files: ['./app/*.html'],
  server: {
    baseDir: ['./dist'],
    index: formatPath({
      dir: relativePath('./dist', './app'),
      base: './index.html',
    }),
  },
};

/** @type {import('browser-sync').Options} */
const prodSource = {
  ui: false,
  server: {
    baseDir: ['./dist'],
  },
  port: 9000,
  logPrefix: name.concat(':server'),
  open: false,
  notify: false,
};

module.exports = Object.assign({}, prodSource, NODE_ENV === 'prod' ? {} : devSource);

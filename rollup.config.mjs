import { babel } from '@rollup/plugin-babel';

import esLint from '@rollup/plugin-eslint';

import { nodeResolve } from '@rollup/plugin-node-resolve';

import terser from '@rollup/plugin-terser';

import { env as processEnv } from 'node:process';

/** @type {{NODE_ENV: 'prod'}} */
const { NODE_ENV = 'prod' } = processEnv;

/** @type {import('rollup').RollupOptions} */
const options = {
  input: './app/app.mjs',
  output: {
    file: './dist/app.mjs',
    format: 'module',
  },
  // prettier-ignore
  plugins: [
    esLint(),
    nodeResolve(),
    babel({
      babelHelpers: 'bundled',
    }),
  ].concat(NODE_ENV === 'prod' ? [
    terser({
      keep_fnames: true,
    }),
  ] : []),
};

export { options as default };
